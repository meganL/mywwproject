''' Create webpage for medical search '''

import requests
from pprint import pprint
import os, uuid
import pyodbc
import re
import json
from flask import Flask, redirect, url_for, render_template, request
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient
from bs4 import BeautifulSoup

# Global Variables
icdnames = [['covid','U07'],['insomnia','G47'],['pneumonia','A01'],['essential hypertension','I10'],['sensorineural deafness','H90'],['cardiac diseases','I130-I152']]
titles = []
authors = []
dates = []
urls = []
abstracts = []
icds = []
rxs = []
links = []
rxns = []

app = Flask(__name__)

@app.route("/", methods=["POST","GET"])
def home():
    del links[:], urls[:], titles[:], authors[:], abstracts[:], icds[:], rxs[:]
    suggestions = []
    if request.method == "POST":
        doc = request.form["nm"]
        suggestions = suggester(doc)
        get_data(doc)

    return render_template("search.html", titles=titles, authors=authors, dates=dates, urls=urls, abstracts=abstracts, tlen=len(titles), icds=icds, rxs=rxs, suggestions=suggestions)

def get_data(doc):
    ''' Extract key phrases from the user's search using text analytics '''
    subscription_key = "31e4531244d648ceb00aecb7ea7e67d6"
    endpoint = "https://analyzetexts.cognitiveservices.azure.com"
    keyphrase_url = endpoint + "/text/analytics/v2.1/keyphrases"
    documents = {"documents": [{"id":"1","language":"en","text":str(doc)}]}

    # identify named entity
    headers = {"Ocp-Apim-Subscription-Key": subscription_key}
    response = requests.post(keyphrase_url, headers=headers, json=documents)
    key_phrases = response.json()
    ents = [article['keyPhrases'] for article in key_phrases['documents']]

    try:
        ents = ents[0]

        if str(ents) == "[[]]":
            return 1
        else:
            for name in ents:
                name = name.lower()
                #pubmed_files(name)
                #get_rxnorm(name)
                search_index(name)
    except:
        pass

def get_icd(names):
    ''' Get ICD data from index

    ***** This will now be done in indexer using a custom skill *****

    '''
    server = 'mysqlserver217.database.windows.net'
    database = 'mySampleDatabase'
    username = 'azureadmin'
    password = 'Rareair23'
    exists = False
    found = False
    driver = '{ODBC Driver 17 for SQL Server}'
    cnxn = pyodbc.connect('DRIVER='+driver+';SERVER='+server+';PORT=1433;DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    cursor.execute("SELECT TOP (100) * FROM [dbo].[CodesICD]")
    row = cursor.fetchone()

    while row and not exists:
        match = []
        for name in names:
            exists = False
            name = str(name).lower()
            if (name == row[1]):
                #if not exists:
                # code found --> insert into array
                exists = True
                found = True
                match = [name, row[2]]
                icds.append(match)

            for n in name.split():
                if (n == row[1]):
                    #if not exists:
                    # code found --> insert into array
                    exists = True
                    found = True
                    match = [n, row[2]]
                    icds.append(match)
            #print(match)

        row = cursor.fetchone()

    if not found:
        icds.append([' ',' '])

def get_rxnorm(name):
    ''' Get RxNorm data from API

    ***** This will now be done in indexer using a custom skill *****

    '''
    baseurl = "https://rxnav.nlm.nih.gov/REST/rxcui?name="
    url = baseurl + name
    skip = False

    for x in rxns:
        if name == x[1]:
            skip = True
            break
    if not skip:
        try:
            # web request to get code
            response = requests.request('GET', url)
            rx = re.search(r'(rxnormId>)([0-9]*)(</rx)', response.text)

            if not rx:
                pass
            else:
                # code found --> insert into array
                match = [rx.group(2), name]
                rxns.append(match)
        except:
            print('error')

    for n in name.split():
        for x in rxns:
            if n == x[1]:
                skip = True
                break
        if not skip:
            url = baseurl + n
            try:
                # web request to get code
                response = requests.request('GET', url)
                rx = re.search(r'(rxnormId>)([0-9]*)(</rx)', response.text)

                if not rx:
                    pass
                else:
                    # code found --> insert into array
                    match = [rx.group(2), n]
                    rxns.append(match)
            except:
                print('error')

    #print(rxns)

def pubmed_files(name):
    '''
    This function fills datasource
    and is only used to scrape web

    Never used when app is running
    '''

    # Connect to my storage
    connect_str = "DefaultEndpointsProtocol=https;AccountName=mywwstorage;AccountKey=HlIn8m0AfR/YINbuL51aB9riMY+ghksRPVlHhf5600mUrlHBKz1zbL14c5ycXX94Ja1oXcasguZ6J4Fr8uYUoQ==;EndpointSuffix=core.windows.net"

    # Create the BlobServiceClient object which will be used to create a container client
    blob_service_client = BlobServiceClient.from_connection_string(connect_str)
    # Create a unique name for the container
    container_name = "medfiles"  # + str(uuid.uuid4())
    try:
        # Create the container
        container_client = blob_service_client.create_container(container_name)
    except:
        pass

    # Create a file in local data directory to upload and download
    local_path = "./data"

    # Get links to articles
    url = 'https://pubmed.ncbi.nlm.nih.gov/'
    for i in range(1,10):
        response = requests.get(url + '?term=' + name + '&page=' + str(i))
        soup = BeautifulSoup(response.text, "html.parser")

        for line in soup.findAll('a'):
            link = str(line.get('href'))
            l = re.search(r'/([0-9]+/)', link)
            if not l:
                pass
            else:
                exist = False
                l = l.group(1) # url extension
                for x in links:
                    if (str(url+l) == x):
                        exist = True
                        break
                if not exist:
                    links.append(url+l)
    #print(links)

    for l in links:
        r = requests.get(l)
        s = BeautifulSoup(r.text, "html.parser")

        # Add link to the file
        local_file_name = str(name + str(uuid.uuid4()) + ".txt")
        upload_file_path = os.path.join(local_path, local_file_name)
        file = open(upload_file_path, 'w')
        file.write("URL: " + l)

        file.write('\nDate: ')
        date = re.findall('[0-9]{4} [a-zA-Z]{3} [0-9]{1,2}', s.get_text())
        try:
            file.write(date[0])
        except:
            pass

        file.write("\nTitle: ")
        t = s.title
        file.write(str(t).lstrip('<title>').rstrip('</title>'))

        file.write("\nAuthors: ")
        authorlist = []
        for x in s.findAll('a'):
            try:
                if (x.get('class')[0] == 'full-name'):
                    authorlist.append(x.get_text())
            except:
                pass
        for a in range(len(authorlist)/2):
            try:
                file.write(str(authorlist[a]) + ", ")
            except UnicodeEncodeError:
                file.write(authorlist[a].encode('ascii','ignore').decode('ascii') + ", ")


        file.write("\nAbstract: ")
        abstract = ''
        try:
            for x in s.find(id='en-abstract'):
                txt = ''
                try:
                    try:
                        txt = re.sub('\n','',str(x.get_text()))
                        txt = re.sub(':.[^A-Z0-9]*', ': ', txt)
                    except:
                        txt = (x.get_text())
                    for c in txt:
                        try:
                            if c == '\n':
                                pass
                            else:
                                file.write(c)
                                abstract += c
                        except:
                            pass
                except:
                    pass
        except:
            pass

        ''' This is done in get_rx function
        
        get_data(abstract)
        file.write("\nRxNorm Codes: ")
        for match in rxns:
            file.write(match[0] + " (" + match[1] + "), ")'''

        file.close()

        # Create a blob client using the local file name as the name for the blob
        blob_client = blob_service_client.get_blob_client(container=container_name, blob=local_file_name)
        print("\nUploading to Azure Storage as blob:\n\t" + local_file_name)
        # Upload the created file
        with open(upload_file_path, "rb") as data:
            blob_client.upload_blob(data)

        # Clean up local file
        os.remove(upload_file_path)

def search_index(name):
    ''' Search for all relevant articles indexed in Azure
        - need to make URL the database
            - azure will routinely be crawling webpage to update offline line files
        - query the index for anything that matches with user's search
        - use dictionary to keep track of the returned articles, authors, etc.
    '''

    # Setup
    index_name = "azureblob-index"
    endpoint = "https://wwmedsearch.search.windows.net"
    datasourceConnectionString = "DefaultEndpointsProtocol=https;AccountName=mywwstorage;AccountKey=HlIn8m0AfR/YINbuL51aB9riMY+ghksRPVlHhf5600mUrlHBKz1zbL14c5ycXX94Ja1oXcasguZ6J4Fr8uYUoQ==;EndpointSuffix=core.windows.net"
    headers = {'Content-Type': 'application/json',
               'api-key': '4DDA88469DCC4EBD8A5D4E0203B3CEC6'}
    params = {
        'api-version': '2019-06-30'
    }

    # Query the index to return the contents
    r = requests.get(endpoint + "/indexes/" + index_name + "/docs?api-version=2019-05-06&search=" + name + "&$select=content", headers=headers)
    #pprint(json.dumps(r.json(), indent=1))

    # Query the index to return the contents
    rk = requests.get(endpoint + "/indexes/" + index_name + "/docs?api-version=2019-05-06&search=" + name + "&$select=keyphrases",
        headers=headers)
    # pprint(json.dumps(rk.json(), indent=1))

    for kps in rk.json()['value']:
        filled = False
        try:
            for words in kps['keyphrases']:
                for code in icdnames:
                    if str(words).lower() == code[0]:
                        filled = True
                        icds.append([str(words).lower(), code[1]])
                for word in words.split():
                    for code in icdnames:
                        if str(word).lower() == code[0]:
                            filled = True
                            icds.append([str(word).lower(), code[1]])
            if not filled:
                icds.append([' ',' '])
        except:
            icds.append([' ',' '])
    #print(icds)

    try:
        for article in r.json()['value']:
            urls.append(str(article['content'].splitlines()[0])[5:])
            dates.append((article['content'].splitlines()[1])[6:])
            t = ''
            try:
                t = str(article['content'].splitlines()[2])[7:]
            except UnicodeEncodeError:
                t = (article['content'].encode('ascii','ignore').decode('ascii')).splitlines()[2][7:]
            titles.append(t)
            authors.append((article['content'].splitlines()[3])[9:-2])
            abstracts.append((article['content'].splitlines()[4])[10:])
            rxs.append((article['content'].splitlines()[5])[13:-2])
    except:
        pass

def suggester(preview):
    # Setup
    index_name = "suggester-index"
    endpoint = "https://wwmedsearch.search.windows.net"
    headers = {'Content-Type': 'application/json',
               'api-key': '4DDA88469DCC4EBD8A5D4E0203B3CEC6'}

    # Query the index to return the contents
    r = requests.get(endpoint + "/indexes/" + index_name + "/docs/suggest?search=" + preview + "&$top=10&suggesterName=sg&api-version=2020-06-30", headers=headers)
    #pprint(json.dumps(r.json(), indent=1))

    sgs = []
    for s in r.json()['value']:
        sx = re.search(r'(.*)\',(.*)', str(s).lower()[20:])

        if not sx:
            pass
        else:
            # code found --> insert into array
            sgs.append(sx.group(1))

    return sgs

if __name__ == "__main__":
    #search_index("fluorine")
    #pubmed_files("pneumonia")
    #suggester('caff')
    app.run()
